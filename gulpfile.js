/* eslint-disable require-jsdoc, no-sync, max-statements */
const gulp = require('gulp');
const {CLIEngine} = require('eslint');
const jsdoc2md = require('jsdoc-to-markdown');
const path = require('path');
const fs = require('fs');
const testResults = 'TEST_RESULTS';

const makeTestResultsDir = async function makeTestResultsDir () {
    if (!fs.exists(path.join(__dirname, testResults), (err) => err)) {
        fs.mkdir(path.join(__dirname, testResults), (err) => err);
    }
};

const docs = async function docs () {
    const jsdoc2mdWrite = async function jsdoc2mdWrite (files, filePath, template = null) {
        const jsdoc2mdOptions = {
            files,
            'plugin': 'dmd-readable',
            'no-cache': true
        };

        if (template !== null) {
            jsdoc2mdOptions.template = template;
        }

        await jsdoc2md.render(jsdoc2mdOptions).
            then((output) => {
                fs.writeFileSync(filePath, output);
            });
    };

    await jsdoc2mdWrite(
        [
            './index.js'
        ],
        path.join(__dirname, 'README.md'),
        fs.readFileSync(path.join(__dirname, 'README.hbs'), 'utf8')
    );
};

const eslintReport = async function eslintReport () {
    const cli = new CLIEngine({
        'useEslintrc': true,
        'ignorePattern': `**/${testResults}/**/*.js`
    });
    const htmlReporter = cli.getFormatter('./index.js');
    const junitReporter = cli.getFormatter('junit');
    const report = cli.executeOnFiles([
        '**/*.js'
    ]);

    await fs.writeFile(path.join(__dirname, testResults, 'eslint-report.html'), htmlReporter(report.results), (err) => err);
    await fs.writeFile(path.join(__dirname, testResults, 'TEST-eslint.xml'), junitReporter(report.results), (err) => err);
};

exports.docs = docs;
exports.eslintReport = eslintReport;
exports.test = gulp.series(makeTestResultsDir, eslintReport);
exports.default = gulp.series(makeTestResultsDir, docs, eslintReport);
