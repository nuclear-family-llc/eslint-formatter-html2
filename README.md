# Eslint Formatter HTML2
This is a simple formatter based directly on the included formatter for Eslint HTML. The only difference is a fallback for the URL, hardcoded to the current Eslint docs URL.

# Application Reference
## formatterHTML2

> Based on Julian Laval's work.

**Author**: Julian Laval and Aaron Huggins  

* [formatterHTML2](#module_formatterHTML2)
    * [~pluralize(word, count)](#module_formatterHTML2..pluralize) ⇒ <code>string</code>
    * [~renderSummary(totalErrors, totalWarnings)](#module_formatterHTML2..renderSummary) ⇒ <code>string</code>
    * [~renderColor(totalErrors, totalWarnings)](#module_formatterHTML2..renderColor) ⇒ <code>int</code>
    * [~renderMessages(messages, parentIndex, rulesMeta)](#module_formatterHTML2..renderMessages) ⇒ <code>string</code>
    * [~renderResults(results, rulesMeta)](#module_formatterHTML2..renderResults) ⇒ <code>string</code>


<br><a name="module_formatterHTML2..pluralize"></a>

### formatterHTML2~pluralize(word, count) ⇒ <code>string</code>
> Given a word and a count, append an s if count is not one.

**Returns**: <code>string</code> - The original word with an s on the end if count is not one.  

| Param | Type | Description |
| --- | --- | --- |
| word | <code>string</code> | A word in its singular form. |
| count | <code>int</code> | A number controlling whether word should be pluralized. |


<br><a name="module_formatterHTML2..renderSummary"></a>

### formatterHTML2~renderSummary(totalErrors, totalWarnings) ⇒ <code>string</code>
> Renders text along the template of x problems (x errors, x warnings)

**Returns**: <code>string</code> - The formatted string, pluralized where necessary  

| Param | Type | Description |
| --- | --- | --- |
| totalErrors | <code>string</code> | Total errors |
| totalWarnings | <code>string</code> | Total warnings |


<br><a name="module_formatterHTML2..renderColor"></a>

### formatterHTML2~renderColor(totalErrors, totalWarnings) ⇒ <code>int</code>
> Get the color based on whether there are errors/warnings...

**Returns**: <code>int</code> - The color code (0 = green, 1 = yellow, 2 = red)  

| Param | Type | Description |
| --- | --- | --- |
| totalErrors | <code>string</code> | Total errors |
| totalWarnings | <code>string</code> | Total warnings |


<br><a name="module_formatterHTML2..renderMessages"></a>

### formatterHTML2~renderMessages(messages, parentIndex, rulesMeta) ⇒ <code>string</code>
> Get HTML (table rows) describing the messages.

**Returns**: <code>string</code> - HTML (table rows) describing the messages.  

| Param | Type | Description |
| --- | --- | --- |
| messages | <code>Array</code> | Messages. |
| parentIndex | <code>int</code> | Index of the parent HTML row. |
| rulesMeta | <code>Object</code> | Dictionary containing metadata for each rule executed by the analysis. |


<br><a name="module_formatterHTML2..renderResults"></a>

### formatterHTML2~renderResults(results, rulesMeta) ⇒ <code>string</code>
**Returns**: <code>string</code> - HTML string describing the results.  

| Param | Type | Description |
| --- | --- | --- |
| results | <code>Array</code> | Test results. |
| rulesMeta | <code>Object</code> | Dictionary containing metadata for each rule executed by the analysis. |


* * *

&copy; 2019 Aaron Huggins &lt;aaron@nuclearfamily.llc&gt;. Documented by [jsdoc-to-markdown](https://github.com/75lb/jsdoc-to-markdown).